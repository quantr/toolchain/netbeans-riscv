package hk.quantr.netbeans.riscv.asm.parser;

import hk.quantr.netbeans.riscv.asm.errorhighlight.MessageError;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser.Result;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrParseResult extends Result {

	public  MessageError errorresult =new MessageError();

	public QuantrParseResult(Snapshot _snapshot , MessageError errorresult) {
		super(_snapshot);
		this.errorresult = errorresult;
	}

	@Override
	protected void invalidate() {

	}

}
