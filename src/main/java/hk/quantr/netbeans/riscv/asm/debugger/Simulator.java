/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger;

import hk.quantr.netbeans.riscv.asm.debugger.action.DebugSimulatorAction;
import hk.quantr.netbeans.riscv.asm.parser.QuantrRiscvParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.commons.io.FileUtils;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Simulator {

	Process process;
	InputStream stderr;
	InputStream stdin;
	OutputStream stdout;
	PrintWriter pw;
	boolean running = false;
	String content = "";
	static Simulator simulator;
	public int radix = 16;
	public String address = "";

	public static Simulator getSimulator() {
		if (simulator == null) {
			simulator = new Simulator();
			return simulator;
		} else {
			return simulator;
		}
	}

	public void start() {
		try {
			Path jarPath = Files.createTempFile("riscv_simulator", ".jar");
			FileUtils.copyInputStreamToFile(QuantrRiscvParser.class.getClassLoader().getResourceAsStream("hk/quantr/netbeans/riscv/simulator/riscv_simulator-1.0.jar"), jarPath.toFile());

			Path quantrXMLPath = Files.createTempFile("quantr", ".xml");
			FileUtils.copyInputStreamToFile(QuantrRiscvParser.class.getClassLoader().getResourceAsStream("hk/quantr/netbeans/riscv/simulator/quantr.xml"), quantrXMLPath.toFile());

			Path kernelPath = Files.createTempFile("kernel", null);
			FileUtils.copyInputStreamToFile(QuantrRiscvParser.class.getClassLoader().getResourceAsStream("hk/quantr/netbeans/riscv/simulator/kernel"), kernelPath.toFile());

			String command;
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.contains("windows")) {
				command = "\"" + System.getProperty("java.home") + "/bin/java\" -jar " + jarPath + " --elf " + kernelPath + " -s 0x1000 -d dump.json --init=" + quantrXMLPath;
			} else {
				command = System.getProperty("java.home") + "/bin/java -jar " + jarPath + " --elf " + kernelPath + " -s 0x1000 -d dump.json --init=" + quantrXMLPath;
			}
			System.out.println("Command: " + command);

			process = Runtime.getRuntime().exec(command);
			stdout = process.getOutputStream();
			pw = new PrintWriter(stdout);
			stderr = process.getErrorStream();
			stdin = process.getInputStream();

			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader br = new BufferedReader(new InputStreamReader(stdin));
						content = "";
						String line;
						while ((line = br.readLine()) != null) {
							content += line + "\n";
							if (line.length() == 0) { //delimeter
								content = content.substring(1);
								running = false;
							}
						}
						br.close();
					} catch (IOException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}).start();

//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        BufferedReader br = new BufferedReader(new InputStreamReader(stderr));
//                        String line;
//                        while ((line = br.readLine()) != null) {
//                            System.err.println(line);
//                        }
//                        br.close();
//                    } catch (IOException ex) {
//                        Exceptions.printStackTrace(ex);
//                    }
//                }
//            }).start();
			xmlOn();
			updateRegister();
			updateMemory("");

		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	public void sendCommand(String str) {
		running = true;
		pw.println(str);
		pw.flush();
	}

	public void waitMe() {
		int timeout = 0;
		while (true) {
			try {
				Thread.sleep(500);
				timeout++;
				if (!running) {
					break;
				} else if (timeout > 10) {
					System.out.println("Time out!!!");
					return;
				}
			} catch (InterruptedException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
	}

	public void updateRegister() {
		sendCommand("r");
		waitMe();
		DebugSimulatorAction.debugRegister(content);
		content = "";
	}
	
	public void updateMemory(String addr) {
		String rad;
		switch(radix) {
			case 2:
				rad = "t"; break;
			case 8:
				rad = "o"; break;
			case 10:
				rad = "d"; break;
			case 16:
				rad = "x"; break;
			default:
				rad = "x";
		}
		sendCommand("x /160b" + rad + " " + addr);
		address = addr;
		waitMe();
		DebugSimulatorAction.debugMemory(content);
		content = "";
	}
	

	public void xmlOn() {
		sendCommand("xml on");
		waitMe();
		content = "";
	}

	public void stepOver() {
		sendCommand("s");
		waitMe();
		content = "";
		updateRegister();
		updateMemory(""); //leave it blank to laod memory from current pc
	}
	public void Continue() {
		sendCommand("c");
		waitMe();
		content = "";
		updateRegister();
		updateMemory(""); //leave it blank to laod memory from current pc
	}
	public void pause() {
		sendCommand("p");
		waitMe();
		content = "";
		updateRegister();
		updateMemory(""); //leave it blank to laod memory from current pc
	}
	
}
