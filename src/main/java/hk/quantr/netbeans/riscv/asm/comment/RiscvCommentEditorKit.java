/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.riscv.asm.comment;

import javax.swing.Action;
import javax.swing.text.TextAction;
import org.netbeans.modules.editor.NbEditorKit;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class RiscvCommentEditorKit extends NbEditorKit {

//	@Override
//	public Action getActionByName(String name) {
//		if (commentAction.equals(name)) {
//			return new CommentAction("//");
//		}
//		if (uncommentAction.equals(name)) {
//			return new UncommentAction("//");
//		}
//		return super.getActionByName(name);
//	}

	@Override
	protected Action[] createActions() {
		Action[] actions = new Action[]{
			new ToggleCommentAction("//")
		};
		return TextAction.augmentList(super.createActions(), actions);
	}

	@Override
	public String getContentType() {
		return "text/x-asm";
	}

}
