package hk.quantr.netbeans.riscv.asm.syntax;

import hk.quantr.netbeans.riscv.ModuleLib;
import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import java.io.IOException;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class AsmEditorLexer implements Lexer<AsmTokenId> {

	private final LexerRestartInfo<AsmTokenId> info;
	private final RISCVAssemblerLexer lexer;

	public AsmEditorLexer(LexerRestartInfo<AsmTokenId> info) throws IOException {
		this.info = info;
		AsmCharStream charStream = new AsmCharStream(info.input(), "RiscvEditor");
		lexer = new RISCVAssemblerLexer(charStream);
	}

	@Override
	public Token<AsmTokenId> nextToken() {
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		if (token.getType() != RISCVAssemblerLexer.EOF) {
			String tokenName = AsmLanguageHierarchy.getToken(token.getType()).name().replaceAll("'", "");
			if (token.getType() >= RISCVAssemblerLexer.RNE && token.getType() <= RISCVAssemblerLexer.MHPMEVENT31) { //REGISTERS
				tokenName = "REGISTERS";
			} else if (token.getType() >= RISCVAssemblerLexer.SLL && token.getType() <= RISCVAssemblerLexer.VWSUBWX) { //INSTRUCTION
				tokenName = "INSTRUCTION";
			} else if (token.getType() >= RISCVAssemblerLexer.DOUBLE_QUOTATION && token.getType() <= RISCVAssemblerLexer.DOTSTRING) { //MACRO
				tokenName = "MACRO";
			}
			if (ModuleLib.isDarkTheme()) {
				AsmTokenId darkTokenId = AsmLanguageHierarchy.getToken("DARK_" + tokenName);
				return info.tokenFactory().createToken(darkTokenId);
			} else {
				AsmTokenId tokenId = AsmLanguageHierarchy.getToken(tokenName);
				return info.tokenFactory().createToken(tokenId);
			}
		}
		if (info.input().readLength()>0){
		AsmTokenId tokenId = AsmLanguageHierarchy.getToken(RISCVAssemblerLexer.NL);
		return info.tokenFactory().createToken(tokenId , info.input().readLength());
		}
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
