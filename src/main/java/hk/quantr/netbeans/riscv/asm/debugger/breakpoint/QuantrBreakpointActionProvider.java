/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger.breakpoint;

import hk.quantr.netbeans.riscv.asm.debugger.DebuggerUtils;
import java.util.Collections;
import java.util.Set;
import org.netbeans.api.debugger.ActionsManager;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.spi.debugger.ActionsProviderSupport;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrBreakpointActionProvider extends ActionsProviderSupport {

	private final static Set ACTIONS = Collections.singleton(ActionsManager.ACTION_TOGGLE_BREAKPOINT);

	@Override
	public void doAction(Object o) {
		System.out.println("QuantrBreakpointActionProvider");

		Breakpoint[] breakpoints = DebuggerManager.getDebuggerManager().getBreakpoints();
		for (Breakpoint bp : breakpoints) {
			System.out.println("bp=" + bp);
		}
		QuantrBreakpoint bp = DebuggerUtils.getBreakpointAtLine();
		DebuggerManager.getDebuggerManager().addBreakpoint(bp);
	}

	@Override
	public Set getActions() {
		return ACTIONS;
	}

}
