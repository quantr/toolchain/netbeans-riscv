/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger.breakpoint;

import org.netbeans.api.debugger.Breakpoint;
import org.openide.filesystems.FileObject;
import org.openide.text.Line;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrBreakpoint extends Breakpoint {

	private Line line;
	private FileObject file;

	public QuantrBreakpoint(Line line, FileObject fo) {
		this.line = line;
		file = fo;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void disable() {
	}

	@Override
	public void enable() {
	}

	public Line getLine() {
		return line;
	}

	public FileObject getFileObject() {
		return file;
	}

	@Override
	public String toString() {
		return "QUANTR: " + file.getPath() + ", " + line.getLineNumber();
	}

	public int getLineNumber() {
		return getLine().getLineNumber() + 1;
	}
}
