package hk.quantr.netbeans.riscv.asm.syntax;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.openide.util.Exceptions;

public class AsmLanguageHierarchy extends LanguageHierarchy<AsmTokenId> {

	private final static List<AsmTokenId> tokens = new ArrayList<>();
	private final static Map<Integer, AsmTokenId> idToTokens = new HashMap<>();

	static {
		int index = 0;
		for (int x = 0; x <= RISCVAssemblerLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = RISCVAssemblerLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
//				name = RiscvLexer.ruleNames[x];
			}
			name = name.replaceAll("'", "");
			AsmTokenId token = new AsmTokenId(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}

		for (int x = 0; x <= RISCVAssemblerLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = RISCVAssemblerLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
			}
			name = name.replaceAll("'", "");

			AsmTokenId token = new AsmTokenId("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized AsmTokenId getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized AsmTokenId getToken(String name) {
		for (Map.Entry<Integer, AsmTokenId> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<AsmTokenId> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<AsmTokenId> createLexer(LexerRestartInfo<AsmTokenId> info) {
		try {
			return new AsmEditorLexer(info);
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
			return null;
		}
	}

	@Override
	protected String mimeType() {
		return "text/x-asm";
	}
}
