/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger;

import hk.quantr.netbeans.riscv.asm.debugger.breakpoint.QuantrBreakpoint;
import javax.swing.JEditorPane;
import javax.swing.text.Caret;
import javax.swing.text.StyledDocument;
import org.openide.cookies.EditorCookie;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.text.Line;
import org.openide.text.NbDocument;
import org.openide.windows.TopComponent;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class DebuggerUtils {

	private final static String ASM_FILE_EXTENSION = "asm";

	public static Line getCurrentLine() {
		Node[] nodes = TopComponent.getRegistry().getCurrentNodes();
		if (nodes == null) {
			return null;
		}
		if (nodes.length != 1) {
			return null;
		}
		Node n = nodes[0];
		FileObject fo = (FileObject) n.getLookup().lookup(FileObject.class);
		if (fo == null) {
			DataObject dobj = (DataObject) n.getLookup().lookup(DataObject.class);
			if (dobj != null) {
				fo = dobj.getPrimaryFile();
			}
		}
		if (fo == null) {
			return null;
		}
		if (!isQuorumSource(fo)) {
			return null;
		}
		LineCookie lineCookie = (LineCookie) n.getCookie(LineCookie.class);
		if (lineCookie == null) {
			return null;
		}
		EditorCookie editorCookie = (EditorCookie) n.getCookie(EditorCookie.class);
		if (editorCookie == null) {
			return null;
		}
		JEditorPane jEditorPane = getEditorPane(editorCookie);
		if (jEditorPane == null) {
			return null;
		}
		StyledDocument document = editorCookie.getDocument();
		if (document == null) {
			return null;
		}
		Caret caret = jEditorPane.getCaret();
		if (caret == null) {
			return null;
		}
		int lineNumber = NbDocument.findLineNumber(document, caret.getDot());
		try {
			Line.Set lineSet = lineCookie.getLineSet();
			assert lineSet != null : lineCookie;
			return lineSet.getCurrent(lineNumber);
		} catch (IndexOutOfBoundsException ex) {
			return null;
		}
	}

	public static FileObject getFileInEditor() {
		Node[] nodes = TopComponent.getRegistry().getCurrentNodes();
		if (nodes == null) {
			return null;
		}
		if (nodes.length != 1) {
			return null;
		}
		Node n = nodes[0];
		FileObject fo = (FileObject) n.getLookup().lookup(FileObject.class);
		if (fo == null) {
			DataObject dobj = (DataObject) n.getLookup().lookup(DataObject.class);
			if (dobj != null) {
				fo = dobj.getPrimaryFile();
			}
		}
		return fo;
	}

	public static QuantrBreakpoint getBreakpointAtLine() {
		Line line = getCurrentLine();
		FileObject fo = getFileInEditor();

		QuantrBreakpoint breakpoint = new QuantrBreakpoint(line, fo);
		return breakpoint;
	}

	public static boolean isQuorumSource(FileObject fo) {
		if (fo.getExt().equals(ASM_FILE_EXTENSION)) {
			return true;
		}
		return false;
	}

	private static JEditorPane getEditorPane(EditorCookie editorCookie) {
		JEditorPane[] op = editorCookie.getOpenedPanes();
		if ((op == null) || (op.length < 1)) {
			return null;
		}
		return op[0];
	}
}
