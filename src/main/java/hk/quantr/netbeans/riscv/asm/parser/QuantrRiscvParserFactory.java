package hk.quantr.netbeans.riscv.asm.parser;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.ParserFactory;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrRiscvParserFactory extends ParserFactory {

	@Override
	public org.netbeans.modules.parsing.spi.Parser createParser(Collection<Snapshot> clctn) {
		return new QuantrRiscvParser();
	}

}
