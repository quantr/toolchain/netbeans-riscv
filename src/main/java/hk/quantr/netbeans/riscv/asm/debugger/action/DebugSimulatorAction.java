package hk.quantr.netbeans.riscv.asm.debugger.action;

import hk.quantr.netbeans.riscv.asm.debugger.Simulator;
import hk.quantr.netbeans.riscv.window.MemoryTableModel;
import hk.quantr.netbeans.riscv.window.MemoryTopComponent;
import hk.quantr.netbeans.riscv.window.RegisterTopComponent;
import java.io.StringReader;
import java.math.BigInteger;
import javax.swing.SwingUtilities;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author darren
 */
public class DebugSimulatorAction {

	public static void debugRegister(String content) {
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(content)));
			xmlDocument.getDocumentElement().normalize();
			XPath xPath = XPathFactory.newInstance().newXPath();
			String expression = "/registers";
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

			for (int x = 0; x < nodeList.getLength(); x++) {
				Node n = nodeList.item(x);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;
					SwingUtilities.invokeLater(() -> {
						TopComponent tc = WindowManager.getDefault().findTopComponent("RegisterTopComponent");
						RegisterTopComponent registerTopComponent = (RegisterTopComponent) tc;
						registerTopComponent.writeRegister("PCTextField", e.getElementsByTagName("pc").item(0).getTextContent());
						for (int i = 0; i < 32; i++) { //load all register data into register window
							registerTopComponent.writeRegister("x" + i + "TextField", e.getElementsByTagName("x" + i).item(0).getTextContent());
						}
						registerTopComponent.writeRegister("satpTextField", e.getElementsByTagName("satp").item(0).getTextContent());
						registerTopComponent.writeRegister("mipTextField", e.getElementsByTagName("mip").item(0).getTextContent());
						registerTopComponent.writeRegister("mstatusTextField", e.getElementsByTagName("mstatus").item(0).getTextContent());
						registerTopComponent.writeRegister("mepcTextField", e.getElementsByTagName("mepc").item(0).getTextContent());
						registerTopComponent.writeRegister("medelegTextField", e.getElementsByTagName("medeleg").item(0).getTextContent());
						registerTopComponent.writeRegister("midelegTextField", e.getElementsByTagName("mideleg").item(0).getTextContent());


					});

				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("Failed to Update Registers");
		}
	}

	public static byte stringToByte(String s, int radix) {
		int numRepresentation = Integer.parseInt(s, radix);
		BigInteger bigInt = BigInteger.valueOf(numRepresentation);
		byte data = bigInt.byteValue();
		return data;
	}

	public static void debugMemory(String content) {
		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			Document xmlDocument = builder.parse(new InputSource(new StringReader(content)));
			xmlDocument.getDocumentElement().normalize();
			XPath xPath = XPathFactory.newInstance().newXPath();
			String expression = "/memory/address";
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);
			String offset = ((Element) nodeList.item(0)).getAttribute("value").substring(2);
			long addr = new BigInteger(offset, 16).longValue();
			MemoryTableModel model = MemoryTopComponent.memoryTableModel;
			for (int x = 0; x < nodeList.getLength(); x++) {
				Node n = nodeList.item(x);
				String[] mem = n.getTextContent().split(" ");
				byte[] asciiString = new byte[8];
				offset = String.format("%08x", (addr + x * 8));
				model.setValueAt("0x" + offset, 0, x);
				for (int i = 0; i < 8; i++) {
					model.setValueAt(mem[i], i + 1, x);
					asciiString[i] = stringToByte(mem[i], Simulator.getSimulator().radix);
				}
				model.setValueAt(new String(asciiString, "UTF-8"), 9, x);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(content);
			System.err.println("Failed to Update Memory");
		}

	}

}
