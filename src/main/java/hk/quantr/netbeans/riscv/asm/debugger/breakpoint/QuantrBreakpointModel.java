package hk.quantr.netbeans.riscv.asm.debugger.breakpoint;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.netbeans.spi.viewmodel.ModelEvent;
import org.netbeans.spi.viewmodel.ModelListener;
import org.netbeans.spi.viewmodel.NodeModel;
import org.netbeans.spi.viewmodel.UnknownTypeException;

public final class QuantrBreakpointModel implements NodeModel {

	public static final String BREAKPOINT
			= "org/netbeans/modules/debugger/resources/breakpointsView/NonLineBreakpoint"; // NOI18N
	public static final String DISABLED_BREAKPOINT
			= "org/netbeans/modules/debugger/resources/breakpointsView/DisabledNonLineBreakpoint"; // NOI18N
	public static final String LINE_BREAKPOINT
			= "org/netbeans/modules/debugger/resources/breakpointsView/Breakpoint"; // NOI18N
	public static final String DISABLED_LINE_BREAKPOINT
			= "org/netbeans/modules/debugger/resources/breakpointsView/DisabledBreakpoint"; // NOI18N

	private List<ModelListener> listeners = new CopyOnWriteArrayList<ModelListener>();

	// NodeModel implementation ................................................
	public String getDisplayName(Object node) throws UnknownTypeException {
		if (node instanceof QuantrBreakpoint) {
			QuantrBreakpoint breakpoint = (QuantrBreakpoint) node;
			return breakpoint.toString();
		}
		throw new UnknownTypeException(node);
	}

	@Override
	public String getIconBase(Object node) throws UnknownTypeException {
		if (node instanceof QuantrBreakpoint) {
			if (!((QuantrBreakpoint) node).isEnabled()) {
				return DISABLED_LINE_BREAKPOINT;
			}
			return LINE_BREAKPOINT;
		}
		throw new UnknownTypeException(node);
	}

	public String getShortDescription(Object node) throws UnknownTypeException {
		if (node instanceof QuantrBreakpoint) {
			QuantrBreakpoint breakpoint = (QuantrBreakpoint) node;
			return breakpoint.getLine().getDisplayName();
		}
		throw new UnknownTypeException(node);
	}

	public void addModelListener(ModelListener l) {
		listeners.add(l);
	}

	public void removeModelListener(ModelListener l) {
		listeners.remove(l);
	}

	public void fireChanges() {
		for (ModelListener ml : listeners) {
			ml.modelChanged(new ModelEvent.TreeChanged(this));
		}
	}

}
