/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger;

import java.beans.PropertyChangeEvent;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerEngine;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.LazyDebuggerManagerListener;
import org.netbeans.api.debugger.Properties;
import org.netbeans.api.debugger.Session;
import org.netbeans.api.debugger.Watch;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class PersistenceManager implements LazyDebuggerManagerListener {

	private static final String QUANTR_DEBUGGER = "debugger";
	private static final String QUANTR_BREAKPOINTS = "QuantrBreakpoints";

	@Override
	public String[] getProperties() {
		return new String[]{
			DebuggerManager.PROP_BREAKPOINTS_INIT,
			DebuggerManager.PROP_BREAKPOINTS
		/*DebuggerManager.PROP_SESSIONS,
            DebuggerManager.PROP_CURRENT_ENGINE,
            DebuggerManager.PROP_DEBUGGER_ENGINES,
            DebuggerManager.PROP_WATCHES,
            DebuggerManager.PROP_WATCHES_INIT*/
		};
	}

	@Override
	public Breakpoint[] initBreakpoints() {
		Properties p = Properties.getDefault().getProperties(QUANTR_DEBUGGER).getProperties(DebuggerManager.PROP_BREAKPOINTS);
		Breakpoint[] breakpoints = (Breakpoint[]) p.getArray(QUANTR_BREAKPOINTS, new Breakpoint[0]);
//		ArrayList<Breakpoint> validBreakpoints = new ArrayList<>();
		for (int i = 0; i < breakpoints.length; i++) {
//			Breakpoint breakpoint = breakpoints[i];
//			if (breakpoint != null) {
//				breakpoint.addPropertyChangeListener(this);
//				validBreakpoints.add(breakpoint);
//			}
			breakpoints[i].addPropertyChangeListener(this);
		}
		return breakpoints;
//		return validBreakpoints.toArray(new Breakpoint[validBreakpoints.size()]);
	}

	@Override
	public void breakpointAdded(Breakpoint breakpoint) {
		Properties properties = Properties.getDefault().getProperties(QUANTR_DEBUGGER).getProperties(DebuggerManager.PROP_BREAKPOINTS);
		properties.setArray(QUANTR_BREAKPOINTS, getBreakpoints());
		breakpoint.addPropertyChangeListener(this);

	}

	@Override
	public void breakpointRemoved(Breakpoint breakpoint) {
		Properties properties = Properties.getDefault().getProperties(QUANTR_DEBUGGER).getProperties(DebuggerManager.PROP_BREAKPOINTS);
		properties.setArray(QUANTR_BREAKPOINTS, getBreakpoints());
		breakpoint.removePropertyChangeListener(this);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource() instanceof Breakpoint) {
			Properties.getDefault().getProperties(QUANTR_DEBUGGER).getProperties(DebuggerManager.PROP_BREAKPOINTS).setArray(QUANTR_BREAKPOINTS, getBreakpoints());
		}
	}

	private Breakpoint[] getBreakpoints() {
		return DebuggerManager.getDebuggerManager().getBreakpoints();
	}

	@Override
	public void initWatches() {
	}

	@Override
	public void watchAdded(Watch watch) {
	}

	@Override
	public void watchRemoved(Watch watch) {
	}

	@Override
	public void sessionAdded(Session session) {
	}

	@Override
	public void sessionRemoved(Session session) {
	}

	@Override
	public void engineAdded(DebuggerEngine engine) {
	}

	@Override
	public void engineRemoved(DebuggerEngine engine) {
	}
}
