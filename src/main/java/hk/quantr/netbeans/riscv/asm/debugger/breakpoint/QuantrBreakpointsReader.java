package hk.quantr.netbeans.riscv.asm.debugger.breakpoint;

import org.netbeans.api.debugger.Properties;

public final class QuantrBreakpointsReader implements Properties.Reader {

	@Override
	public String[] getSupportedClassNames() {
		return new String[]{
			QuantrBreakpoint.class.getName(),};
	}

	@Override
	public Object read(final String typeID, final Properties props) {
		System.out.println("read");
		return null;
	}

	@Override
	public void write(final Object object, final Properties props) {
		System.out.println("write");
	}

}
