package hk.quantr.netbeans.riscv.asm.errorhighlight;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Message")
public class Message {

	public String type;
	public String message;
	public int lineNumber;
	public int startPosition;
	public int endPosition;
	public String line;

	public Message(String type, String message, int lineNumber, int startPosition, int endPosition, String line) {
		this.type = type;
		this.message = message;
		this.lineNumber = lineNumber;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
		this.line = line;
	}
}
