package hk.quantr.netbeans.riscv.asm.parser;

import com.thoughtworks.xstream.XStream;
import hk.quantr.javalib.CommonLib;

import hk.quantr.netbeans.riscv.asm.errorhighlight.MessageError;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import javax.swing.event.ChangeListener;
import org.antlr.v4.runtime.Token;
import org.apache.commons.io.FileUtils;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrRiscvParser extends Parser {

	private Snapshot snapshot;
	private QuantrParseResult result;

	public static ArrayList<Token> allTokens = new ArrayList<>();
	public static MessageError Error = new MessageError();

	@Override
	public void parse(Snapshot snapshot, Task task, SourceModificationEvent sme) throws ParseException {
		try {
			InputStream GetAssembler = QuantrRiscvParser.class.getClassLoader().getResourceAsStream("hk/quantr/netbeans/riscv/assembler-1.5.jar");
			Path jarPath = Files.createTempFile("assembler", ".jar");
			FileUtils.copyInputStreamToFile(GetAssembler, jarPath.toFile());
			String content = snapshot.getText().toString();
			Path temp = Files.createTempFile("temp", ".s");
			FileWriter file = new FileWriter(temp.toFile());
			try ( BufferedWriter writer = new BufferedWriter(file)) {
				writer.write(content);
			}
			String command;
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.contains("windows")) {
				command = "\"" + System.getProperty("java.home") + "/bin/java\" -jar " + jarPath + " -a rv64 -x " + temp;
			} else {
				command = System.getProperty("java.home") + "/bin/java -jar " + jarPath + " -a rv64 -x " + temp;
			}

			String xmlresult = CommonLib.runCommand(command);
//			System.out.println("xmlresult: " + xmlresult);
			temp.toFile().delete();
			if (!xmlresult.isEmpty()) {
				XStream xstream = new XStream();
				xstream.processAnnotations(MessageError.class);
				Error = (MessageError) xstream.fromXML(xmlresult);
				result = new QuantrParseResult(snapshot, Error);
			} else {
				Error.Messages.clear();
			}
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	@Override
	public Parser.Result getResult(Task task) throws ParseException {
		return result;
	}

	@Override
	public void addChangeListener(ChangeListener cl) {
	}

	@Override
	public void removeChangeListener(ChangeListener cl) {
	}

}
