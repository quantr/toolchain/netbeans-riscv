package hk.quantr.netbeans.riscv.asm.errorhighlight;

import hk.quantr.netbeans.riscv.ModuleLib;
import hk.quantr.netbeans.riscv.asm.parser.QuantrRiscvParser;
import java.util.ArrayList;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ErrorHighlightingTask extends ParserResultTask {

	public static ArrayList<Message> Messages = new ArrayList<>();

	@Override
	public void run(Result result, SchedulerEvent event) {
		ModuleLib.log("ErrorHighlightingTask");
		MessageError Error = QuantrRiscvParser.Error;
		Document document = result.getSnapshot().getSource().getDocument(false);
		ArrayList<ErrorDescription> errors = new ArrayList();

		for (Message ErrorMessage : Error.Messages) {
			int start = ErrorMessage.startPosition, stop = (ErrorMessage.endPosition) + 1;
			if (ErrorMessage.type.equals("Syntax Error") || ErrorMessage.type.equals("Encoder Error")) {
				try {
					ErrorDescription errorDescription;
					if (ErrorMessage.type.equals("Syntax Error")) {
						errorDescription = ErrorDescriptionFactory.createErrorDescription(
								Severity.ERROR,
								ErrorMessage.message,
								document,
								document.createPosition(start),
								document.createPosition(stop)
						);
					} else {
						errorDescription = ErrorDescriptionFactory.createErrorDescription(
								Severity.WARNING,
								ErrorMessage.message,
								document,
								document.createPosition(start),
								document.createPosition(stop)
						);
					}
					ModuleLib.log("errorDescription=" + errorDescription);
					errors.add(errorDescription);
				} catch (BadLocationException ex) {
					Exceptions.printStackTrace(ex);
				}
			}
		}
		HintsController.setErrors(document, "Riscv", errors);
	}

	@Override
	public int getPriority() {
		return 100;
	}

	@Override
	public Class getSchedulerClass() {
		return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
	}

	@Override
	public void cancel() {
	}

}
