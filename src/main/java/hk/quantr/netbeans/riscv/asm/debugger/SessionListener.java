/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger;

import java.beans.PropertyChangeListener;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.DebuggerManagerAdapter;
import org.netbeans.api.debugger.Session;
import org.openide.text.AnnotationProvider;
import org.openide.text.Line;
import org.openide.util.Lookup;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SessionListener extends DebuggerManagerAdapter implements PropertyChangeListener, AnnotationProvider{

    @Override
    public void sessionAdded( Session session ) {
        super.sessionAdded(session);
    }

    @Override
    public void sessionRemoved( Session session ) {
        super.sessionRemoved(session);
    }

	@Override
	public void annotate(Line.Set set, Lookup lkp) {
		 DebuggerManager.getDebuggerManager().getBreakpoints();
	}

}
