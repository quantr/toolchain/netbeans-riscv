package hk.quantr.netbeans.riscv.asm.hyperlink;


import hk.quantr.netbeans.riscv.asm.parser.QuantrRiscvParser;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.antlr.v4.runtime.Token;

import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@MimeRegistration(mimeType = "text/x-asm", service = HyperlinkProvider.class)
public class AsmHyperlinkProvider implements HyperlinkProvider {

	@Override
	public boolean isHyperlinkPoint(Document dcmnt, int i) {

		for (Token token : QuantrRiscvParser.allTokens) {
			if (token.getStartIndex() <= i && i <= token.getStopIndex()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int[] getHyperlinkSpan(Document dcmnt, int i) {
		for (Token token : QuantrRiscvParser.allTokens) {
			if (token.getStartIndex() <= i && i <= token.getStopIndex()) {
				return new int[]{token.getStartIndex(), token.getStopIndex() + 1};
			}
		}
		return null;
	}

	@Override
	public void performClickAction(Document dcmnt, int i) {
//		ModuleLib.log("performClickAction " + i);
		JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
		
				jTextComponent.setCaretPosition((100));
			
	
			
	}

}
