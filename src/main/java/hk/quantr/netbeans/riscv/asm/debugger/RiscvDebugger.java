/*
 * Copyright 2022 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.riscv.asm.debugger;

import hk.quantr.netbeans.riscv.asm.debugger.breakpoint.QuantrBreakpoint;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerInfo;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.spi.debugger.ActionsProviderSupport;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.debugger.DebuggerEngineProvider;
import org.netbeans.spi.debugger.SessionProvider;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class RiscvDebugger extends ActionsProviderSupport {

	public static final Object ACTION_STEP_OVER = "stepOver";
	public static final Object ACTION_STEP_BACK_OVER = "stepBackOver";
	public static final Object ACTION_RUN_INTO_METHOD = "runIntoMethod";
	public static final Object ACTION_STEP_INTO = "stepInto";
	public static final Object ACTION_STEP_BACK_INTO = "stepBackInto";
	public static final Object ACTION_STEP_OUT = "stepOut";
	public static final Object ACTION_STEP_OPERATION = "stepOperation";
	public static final Object ACTION_CONTINUE = "Continue";
	public static final Object ACTION_REWIND = "rewind";
	public static final Object ACTION_START = "start";
	public static final Object ACTION_KILL = "kill";
	public static final Object ACTION_MAKE_CALLER_CURRENT = "makeCallerCurrent";
	public static final Object ACTION_MAKE_CALLEE_CURRENT = "makeCalleeCurrent";
	public static final Object ACTION_PAUSE = "pause";
	public static final Object ACTION_RUN_TO_CURSOR = "runToCursor";
	public static final Object ACTION_RUN_BACK_TO_CURSOR = "runBackToCursor";
	public static final Object ACTION_POP_TOPMOST_CALL = "popTopmostCall";
	public static final Object ACTION_FIX = "fix";
	public static final Object ACTION_RESTART = "restart";
	public static final Object ACTION_TOGGLE_BREAKPOINT = "toggleBreakpoint";

	private RiscvEngineProvider engineProvider;
	private static final Set actions = new HashSet();

	static {
		actions.add(ACTION_RUN_BACK_TO_CURSOR);
		actions.add(ACTION_STEP_BACK_INTO);
		actions.add(ACTION_STEP_BACK_OVER);
		actions.add(ACTION_REWIND);
		actions.add(ACTION_KILL);
		actions.add(ACTION_PAUSE);
		actions.add(ACTION_CONTINUE);
		actions.add(ACTION_START);
		actions.add(ACTION_STEP_INTO);
		actions.add(ACTION_STEP_OVER);
		actions.add(ACTION_RUN_TO_CURSOR);
		actions.add(ACTION_TOGGLE_BREAKPOINT);
	}

	public static final String RISCV_DEBUGGER_INFO = "RiscvDebuggerInfo";
	public static final String RISCV_SESSION = "RiscvSession";

	public RiscvDebugger(ContextProvider contextProvider) {
		engineProvider = (RiscvEngineProvider) contextProvider.lookupFirst(null, DebuggerEngineProvider.class);
		for (Iterator it = actions.iterator(); it.hasNext();) {
			setEnabled(it.next(), true);
		}
	}

	public static void startDebugger() {
		DebuggerManager manager = DebuggerManager.getDebuggerManager();
		System.out.println("Im in debugger");
		DebuggerInfo info = DebuggerInfo.create(RISCV_DEBUGGER_INFO,
				new Object[]{
					new SessionProvider() {

				@Override
				public String getSessionName() {
					return "Riscv Program";
				}

				@Override
				public String getLocationName() {
					return "localhost";
				}

				public String getTypeID() {
					return RISCV_SESSION;
				}

				public Object[] getServices() {
					return new Object[]{};
				}
			}, null
				});

		manager.startDebugging(info);
	}

	@Override
	public void doAction(Object action) {
		if (action == ACTION_RUN_BACK_TO_CURSOR) {
		} else if (action == ACTION_STEP_BACK_INTO) {
		} else if (action == ACTION_STEP_BACK_OVER) {
		} else if (action == ACTION_REWIND) {
		} else if (action == ACTION_KILL) {
			//this stops the debugger
			engineProvider.getDestructor().killEngine();
		} else if (action == ACTION_PAUSE) {
			Simulator.getSimulator().pause();
		} else if (action == ACTION_CONTINUE) {
			Simulator.getSimulator().Continue();
		} else if (action == ACTION_START) {
		} else if (action == ACTION_STEP_INTO) {
		} else if (action == ACTION_STEP_OVER) {
			Simulator.getSimulator().stepOver();
		} else if (action == ACTION_RUN_TO_CURSOR) {
		} else if (action == ACTION_TOGGLE_BREAKPOINT) {
			System.out.println("ACTION_TOGGLE_BREAKPOINT");

			SwingUtilities.invokeLater(() -> {
				Breakpoint[] breakpoints = DebuggerManager.getDebuggerManager().getBreakpoints();
				for (Breakpoint bp : breakpoints) {
					System.out.println("bp=" + bp);
				}
				QuantrBreakpoint bp = DebuggerUtils.getBreakpointAtLine();
				DebuggerManager.getDebuggerManager().addBreakpoint(bp);
			});
		}

		//print this out, since we don't actually have an implementation of
		//a Riscv Debugger.
		System.out.println("The debugger took the action: " + action);
	}

	@Override
	public Set getActions() {
		return actions;
	}
}
