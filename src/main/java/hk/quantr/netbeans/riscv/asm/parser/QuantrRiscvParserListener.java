package hk.quantr.netbeans.riscv.asm.parser;

import hk.quantr.assembler.antlr.RISCVAssemblerParserBaseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.antlr.v4.runtime.Parser;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class QuantrRiscvParserListener extends RISCVAssemblerParserBaseListener {

	private final Map<String, Integer> rmap = new HashMap<>();

	public QuantrRiscvParserListener(Parser parser) {
		rmap.clear();
		rmap.putAll(parser.getRuleIndexMap());
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream()
				.filter(e -> Objects.equals(e.getValue(), key))
				.map(Map.Entry::getKey)
				.findFirst()
				.orElse(null);
	}
}
