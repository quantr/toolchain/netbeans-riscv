package hk.quantr.netbeans.riscv.lst.syntaxhighlight;


import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.netbeans.riscv.ModuleLib;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.openide.util.Exceptions;

public class LstLanguageHierarchy extends LanguageHierarchy<LstTokenId> {

	private final static List<LstTokenId> tokens = new ArrayList<>();
	private final static Map<Integer, LstTokenId> idToTokens = new HashMap<>();

	static {
		int index = 0;
		for (int x = 0; x <= RISCVAssemblerLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = RISCVAssemblerLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
//				name = lstLexer.ruleNames[x];
			}
			name = name.replaceAll("'", "");
			ModuleLib.log("name=" + name);
			LstTokenId token = new LstTokenId(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}

		for (int x = 0; x <= RISCVAssemblerLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = RISCVAssemblerLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
//				name = lstLexer.ruleNames[x];
			}
			name = name.replaceAll("'", "");

			LstTokenId token = new LstTokenId("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized LstTokenId getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized LstTokenId getToken(String name) {
		for (Map.Entry<Integer, LstTokenId> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<LstTokenId> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<LstTokenId> createLexer(LexerRestartInfo<LstTokenId> info) {
		try {
			return new LstEditorLexer(info);
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
			return null;
		}
	}

	@Override
	protected String mimeType() {
		return "text/x-lst";
	}
}
