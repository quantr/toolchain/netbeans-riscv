package hk.quantr.netbeans.riscv.lst.syntaxhighlight;

import hk.quantr.assembler.antlr.RISCVAssemblerLexer;
import hk.quantr.netbeans.riscv.ModuleLib;
import java.io.IOException;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class LstEditorLexer implements Lexer<LstTokenId> {

	private final LexerRestartInfo<LstTokenId> info;
	private final RISCVAssemblerLexer lexer;
	private int tokenNumber;

	public LstEditorLexer(LexerRestartInfo<LstTokenId> info) throws IOException {
		this.info = info;
		LstCharStream charStream = new LstCharStream(info.input(), "LstEditor");
		lexer = new RISCVAssemblerLexer(charStream);
	}

	@Override
	public Token<LstTokenId> nextToken() {
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		if (token.getType() != RISCVAssemblerLexer.EOF) {
			String tokenName = LstLanguageHierarchy.getToken(token.getType()).name().replaceAll("'", "");
			if (token.getCharPositionInLine() == 0) { //new line, no = 1
				tokenNumber = 1;
			}
			if (token.getType() >= RISCVAssemblerLexer.RNE && token.getType() <= RISCVAssemblerLexer.MHPMEVENT31) { //REGISTERS
				tokenName = "REGISTERS";
			} else if (token.getType() >= RISCVAssemblerLexer.SLL && token.getType() <= RISCVAssemblerLexer.VWSUBWX) { //INSTRUCTION
				tokenName = "INSTRUCTION";
			} else if (token.getType() >= RISCVAssemblerLexer.DOUBLE_QUOTATION && token.getType() <= RISCVAssemblerLexer.DOTSTRING) { //MACRO
				tokenName = "MACRO";
			} else if (token.getType() == RISCVAssemblerLexer.MATH_EXPRESSION || token.getType() == RISCVAssemblerLexer.IDENTIFIER) {
				if (token.getType() == RISCVAssemblerLexer.MATH_EXPRESSION && tokenNumber == 1) { //no = 1, line number
					tokenName = "LINENUMBER";
				} else if (tokenNumber == 3) { //no = 3, address
					tokenName = "ADDRESS";
				} else if (tokenNumber >= 4 && tokenNumber <= 7) { //4 <= no <= 7, byte
					tokenName = "BYTE";
				}
			}
//			System.out.println(tokenName + " " + token.getCharPositionInLine() + " " + tokenNumber);
			if (token.getType() == RISCVAssemblerLexer.WS || token.getType() == RISCVAssemblerLexer.NL) { //meet WS, no += 1
				tokenNumber += 1;
			}
			if (ModuleLib.isDarkTheme()) {
				LstTokenId darkTokenId = LstLanguageHierarchy.getToken("DARK_" + tokenName);
				return info.tokenFactory().createToken(darkTokenId);
			} else {
				LstTokenId tokenId = LstLanguageHierarchy.getToken(tokenName);
				return info.tokenFactory().createToken(tokenId);
			}
		}
		if (info.input().readLength() > 0) {
			LstTokenId tokenId = LstLanguageHierarchy.getToken(RISCVAssemblerLexer.NL);
			return info.tokenFactory().createToken(tokenId, info.input().readLength());
		}
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
