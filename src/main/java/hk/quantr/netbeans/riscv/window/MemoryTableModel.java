/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.window;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author derek
 */
//http://www.java2s.com/Code/Java/Swing-JFC/CreatingsimpleJTableusingAbstractTableModel.htm
public class MemoryTableModel extends AbstractTableModel {

	public String[] colNames = {"Offset", "+0", "+1", "+2", "+3", "+4", "+5", "+6", "+7", "String"};
	public String[][] data = new String[10][20];

	

	@Override
	public int getRowCount() {
		return 20;
	}

	@Override
	public int getColumnCount() {
		return colNames.length;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = (String) value;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return data[columnIndex][rowIndex];
	}

	@Override
	public String getColumnName(int columnIndex) {
		return colNames[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}
}
