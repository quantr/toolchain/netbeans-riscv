/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.window;

import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import java.math.BigInteger;


/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MemoryTableCellRenderer extends JLabel implements TableCellRenderer {

    public int radix;
	public int default_radix =16;


    public MemoryTableCellRenderer() {
        super();
        setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        if (column == 0 || column == table.getColumnCount() - 1) {
            setHorizontalAlignment(SwingConstants.LEFT);
        } else {
            setHorizontalAlignment(SwingConstants.CENTER);
        }
        if (isSelected) {
            this.setBackground((Color) UIManager.get("Table.selectionBackground"));
        } else {
            this.setBackground((Color) UIManager.get("Table.background"));
        }
//		setBackground(Color.red);
        if (column != 9) {
            if (radix == 2) {
                BigInteger temp = CommonLib.string2BigInteger(((String) value),default_radix);
                String bin = temp.toString(radix);
                System.out.println(value + " " + row + " " + "column: " + column);
                if (((String) value).contains("0x")) {
                    setText(((String) value) + " " + radix);
                } else {
                    setText(bin + " " + radix);
                }

            } else if (radix == 8) {
                BigInteger temp = CommonLib.string2BigInteger(((String) value),default_radix);
                String oct = temp.toString(radix);
                System.out.println(value + " " + row + " " + "column: " + column);

                if (((String) value).contains("0x")) {
                    setText(((String) value) + " " + radix);
                } else {
                    setText(oct + " " + radix);
                }
            } else if (radix == 10) {
                BigInteger temp = CommonLib.string2BigInteger(((String) value),default_radix);
                String dec = temp.toString(radix);
                System.out.println(value + " " + row + " " + "column: " + column);

                if (((String) value).contains("0x")) {
                    setText(((String) value) + " " + radix);
                } else {
                    setText(dec + " " + radix);
                }

            } else if (radix == 16) {
                System.out.println(value + " " + row + " " + "column: " + column);

               
                setText(((String) value) + " " + radix);
            } else {
                setText(null + " " + null);
            }
        } else {
            setText(((String) value) + " " + radix);
        }

        return this;
    }

}
