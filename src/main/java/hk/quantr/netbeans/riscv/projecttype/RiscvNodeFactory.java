/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.projecttype;

/**
 *
 * @author derek
 */
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

@NodeFactory.Registration(projectType = "org-riscv-project", position = 10)
public class RiscvNodeFactory implements NodeFactory {

	@Override
	public NodeList createNodes(Project project) {
		RiscvProject p = project.getLookup().lookup(RiscvProject.class);
		assert p != null;
		return new RiscvNodeList(p);
	}

	private class RiscvNodeList implements NodeList {

		RiscvProject project;

		public RiscvNodeList(RiscvProject project) {
			this.project = project;
		}

		@Override
		public List keys() {
			FileObject RiscvFolder = project.getProjectDirectory().getFileObject("setting.hk");
			List result = new ArrayList();
			if (RiscvFolder != null) {
				for (FileObject textsFolderFile : RiscvFolder.getChildren()) {
					try {
						result.add(DataObject.find(textsFolderFile).getNodeDelegate());
					} catch (DataObjectNotFoundException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}
			return result;
		}

		public Node node(Node node) {
			return new FilterNode(node);
		}

		@Override
		public void addNotify() {
		}

		@Override
		public void removeNotify() {
		}

		@Override
		public void addChangeListener(ChangeListener cl) {
		}

		@Override
		public void removeChangeListener(ChangeListener cl) {
		}

		@Override
		public Node node(Object k) {
			throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
		}

	}

}
