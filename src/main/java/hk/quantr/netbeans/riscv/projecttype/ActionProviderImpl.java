/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package hk.quantr.netbeans.riscv.projecttype;

import hk.quantr.netbeans.riscv.asm.debugger.RiscvDebugger;
import hk.quantr.netbeans.riscv.asm.debugger.Simulator;
import javax.swing.JOptionPane;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.ui.support.DefaultProjectOperations;
import org.openide.util.Lookup;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ActionProviderImpl implements ActionProvider {

    private final String[] supported = new String[]{
        ActionProvider.COMMAND_DELETE,
        ActionProvider.COMMAND_COPY,
        ActionProvider.COMMAND_RENAME,
        ActionProvider.COMMAND_MOVE,
        ActionProvider.COMMAND_DEBUG_SINGLE,
        ActionProvider.COMMAND_DEBUG,
        ActionProvider.COMMAND_DEBUG_STEP_INTO,
        ActionProvider.COMMAND_DEBUG_TEST_SINGLE,
        ActionProvider.COMMAND_RUN,
        ActionProvider.COMMAND_RUN_SINGLE,
        ActionProvider.COMMAND_BUILD,
        ActionProvider.COMMAND_CLEAN,
        ActionProvider.COMMAND_COMPILE_SINGLE
    };

    private RiscvDebugger debugger;
    RiscvProject riscvProject;

    ActionProviderImpl(RiscvProject riscvProject) {
        this.riscvProject = riscvProject;

        debugger = DebuggerManager.getDebuggerManager().lookupFirst(null, RiscvDebugger.class);
    }

    @Override
    public String[] getSupportedActions() {
        return supported;
    }

    public void buildAction() {
        JOptionPane.showMessageDialog(null, "buildAction");
    }

    @Override
    public void invokeAction(String str, Lookup lookup) throws IllegalArgumentException {
        if (str.equalsIgnoreCase(ActionProvider.COMMAND_BUILD)) {
            buildAction();
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_CLEAN)) {
            //clean target file and then
            buildAction();
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_DELETE)) {
            DefaultProjectOperations.performDefaultDeleteOperation(riscvProject);
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_COPY)) {
            DefaultProjectOperations.performDefaultCopyOperation(riscvProject);
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_MOVE)) {
            DefaultProjectOperations.performDefaultMoveOperation(riscvProject);
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_RENAME)) {
            DefaultProjectOperations.performDefaultRenameOperation(riscvProject, null);
        } else if (str.equalsIgnoreCase(ActionProvider.COMMAND_DEBUG)) {
            Simulator.getSimulator().start();
//				debugger = DebuggerManager.getDebuggerManager().lookupFirst(null, RiscvDebugger.class);
            debugger.startDebugger();
        }
    }

    @Override
    public boolean isActionEnabled(String command, Lookup lookup) throws IllegalArgumentException {
        return true;
    }
}
