package hk.quantr.netbeans.riscv.projecttype;

import hk.quantr.javalib.XMLHelper;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.spi.project.ProjectState;

import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ProxyLookup;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.DeleteOperationImplementation;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.ProjectSensitiveActions;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

public class RiscvProject implements Project {

	private final FileObject projectDir;
	private final ProjectState state;
	private Lookup lkp;

	public RiscvProject(FileObject projectDir, ProjectState state) {
		this.projectDir = projectDir;
		this.state = state;
	}

	@Override
	public FileObject getProjectDirectory() {
		return projectDir;
	}

	FileObject getTextFolder(boolean create) {
		FileObject result = projectDir.getFileObject(RiscvProjectFactory.PROJECT_FILE);
		if (result == null && create) {
			try {
				result = projectDir.createFolder(RiscvProjectFactory.PROJECT_FILE);
			} catch (IOException ioe) {
				Exceptions.printStackTrace(ioe);
			}
		}
		return result;
	}

	@Override
	public Lookup getLookup() {
		if (lkp == null) {
			lkp = Lookups.fixed(new Object[]{
				state,
				new ActionProviderImpl(this),
				new Info(this),
				new RiscvDeleteOperation(),
				new RiscvProjectLogicalView(this),
				new RiscvCustomizerProvider(this),

			});
		}
		return lkp;
	}

	private final class RiscvDeleteOperation implements DeleteOperationImplementation {

		@Override
		public void notifyDeleting() throws IOException {
		}

		@Override
		public void notifyDeleted() throws IOException {
		}

		@Override
		public List<FileObject> getMetadataFiles() {
			List<FileObject> dataFiles = new ArrayList<>();
			return dataFiles;
		}

		@Override
		public List<FileObject> getDataFiles() {
			List<FileObject> dataFiles = new ArrayList<>();
			return dataFiles;
		}
	}

	public class Info implements ProjectInformation {

		RiscvProject riscvProject;

		Info(RiscvProject riscvProject) {
			this.riscvProject = riscvProject;
		}
		@StaticResource()
		public static final String RISCV_ICON = "hk/quantr/netbeans/riscv/projecttype/riscv-icon.png";

		@Override
		public Icon getIcon() {
			return new ImageIcon(ImageUtilities.loadImage(RISCV_ICON));
		}

		@Override
		public String getName() {
			return riscvProject.getProjectDirectory().getName();
		}

		@Override
		public String getDisplayName() {
			return getName();
		}

		@Override
		public void addPropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public void removePropertyChangeListener(PropertyChangeListener pcl) {
			//do nothing, won't change
		}

		@Override
		public Project getProject() {
			return riscvProject;
		}

	}

	public class RiscvProjectLogicalView implements LogicalViewProvider {

		@StaticResource()
		public static final String RISCV_ICON = "hk/quantr/netbeans/riscv/projecttype/riscv-icon.png";

		private final RiscvProject project;

		public RiscvProjectLogicalView(RiscvProject project) {
			this.project = project;
		}

		@Override
		public Node createLogicalView() {
			try {
				//Obtain the project directory's node:
				//FileObject hk = project.getTextFolder(true);
				FileObject projectDirectory = project.getProjectDirectory();
				DataFolder projectFolder = DataFolder.findFolder(projectDirectory);
				Node nodeOfProjectFolder = projectFolder.getNodeDelegate();
				//Decorate the project directory's node:
				return new RiscvProjectNode(nodeOfProjectFolder, project);
			} catch (DataObjectNotFoundException donfe) {
				Exceptions.printStackTrace(donfe);
				//Fallback-the directory couldn't be created -
				//read-only filesystem or something evil happened
				return new AbstractNode(Children.LEAF);
			}
		}

		private final class RiscvProjectNode extends FilterNode {

			final RiscvProject project;

			public RiscvProjectNode(Node node, RiscvProject project) throws DataObjectNotFoundException {
				super(node, /*NodeFactorySupport.createCompositeChildren(
						project,
						"Projects/org-riscv-project/Nodes"),*/
						new FilterNode.Children(node),
						new ProxyLookup(
								new Lookup[]{
									Lookups.singleton(project),
									node.getLookup()
								}));
				this.project = project;
			}

			@Override
			public Action[] getActions(boolean arg0) {
				return new Action[]{
					CommonProjectActions.newFileAction(),
					new BuildAction(project, "Build"),
					new BuildAction(project, "Clean"),
					new BuildAction(project, "Clean and Build"),
					ProjectSensitiveActions.projectCommandAction(ActionProvider.COMMAND_DEBUG, "Debug", null),
					CommonProjectActions.closeProjectAction(),
					CommonProjectActions.deleteProjectAction(),
					CommonProjectActions.customizeProjectAction()};
			}

			@Override
			public Image getIcon(int type) {
				return ImageUtilities.loadImage(RISCV_ICON);
			}

			@Override
			public Image getOpenedIcon(int type) {
				return getIcon(type);
			}

			@Override
			public String getDisplayName() {
				try {
					//return project.getProjectDirectory().getName();
					return XMLHelper.evaluateXPath(new File(project.getProjectDirectory().getPath() + File.separator + "pom.xml"), "/project/name/text()").get(0);
				} catch (Exception ex) {
					Exceptions.printStackTrace(ex);
				}
				return "unknown";
			}

		}

		@Override
		public Node findPath(Node root, Object target) {
			//leave unimplemented for now
			return null;
		}

	}
}
