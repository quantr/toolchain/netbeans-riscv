package hk.quantr.netbeans.riscv.projecttype;

import hk.quantr.netbeans.riscv.projecttype.RiscvProject;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import org.apache.commons.lang3.ArrayUtils;
import org.netbeans.modules.maven.options.MavenSettings;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.util.ContextAwareAction;
import org.openide.util.Lookup;

public class BuildAction extends AbstractAction implements ContextAwareAction {

    RiscvProject project;
    String label;
    String osName = System.getProperty("os.name").toLowerCase();
    String command, compileAction;

    public BuildAction(RiscvProject project, String label) {
        super(label);
        this.label = label;
        this.project = project;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        StopAction stopAction = new StopAction("Stop", new ImageIcon(BuildAction.class.getClassLoader().getResource("/hk/quantr/netbeans/riscv/projecttype/action/stop.png")));
        Action actions[] = new Action[]{stopAction};
        InputOutput io;

        if (label.equals("Build")) {
            io = IOProvider.getDefault().getIO("Build (" + project.getProjectDirectory().getName() + ")", false, actions, null);
            compileAction = "compile";
        } else if (label.equals("Clean")) {
            io = IOProvider.getDefault().getIO("Clean (" + project.getProjectDirectory().getName() + ")", false, actions, null);
            compileAction = "clean";
        } else if (label.equals("Clean and Build")) {
            io = IOProvider.getDefault().getIO("Clean and Build (" + project.getProjectDirectory().getName() + ")", false, actions, null);
            compileAction = "clean compile";
        } else {
            return;
        }
        io.setFocusTaken(true);
        command = osName.contains("windows") ? "mvn.cmd" : "mvn";

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    String commandArray[] = ArrayUtils.addAll(new String[]{MavenSettings.getDefaultExternalMavenRuntime() + File.separator + "bin" + File.separator + command}, compileAction.split(" "));
                    Process process = Runtime.getRuntime().exec(commandArray, null, new File(project.getProjectDirectory().getPath()));
                    InputStream stdin = process.getInputStream();

                    BufferedReader br = new BufferedReader(new InputStreamReader(stdin));
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (stopAction.stop) {
                            break;
                        }
                        io.getOut().println(line);
                    }
                    stopAction.stop = false;
                } catch (IOException ex) {
                    io.getOut().println("Error: " + ex.getMessage());
                }
            }
        };

        thread.start();
    }

    @Override
    public Action createContextAwareInstance(Lookup lkp) {
        return this;
    }

}
