package hk.quantr.netbeans.riscv;

import com.thoughtworks.xstream.XStream;
import hk.quantr.javalib.CommonLib;
import hk.quantr.netbeans.riscv.asm.errorhighlight.MessageError;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openide.util.Exceptions;



public class TestRunAssembler {

	@Test
	public void test() {
		try {
			InputStream is = TestRunAssembler.class.getClassLoader().getResourceAsStream("hk/quantr/netbeans/riscv/assembler-1.5.jar");
			Path jarPath = Files.createTempFile("assembler", ".jar");
			FileUtils.copyInputStreamToFile(is, jarPath.toFile());
			String command;
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.toLowerCase().contains("windows")) {
				command = "\"" + System.getProperty("java.home") + "/bin/java\" -jar " + jarPath+ " -a rv64 -l a.lst -o a.bin ../Assembler/a.asm -x";
			} else {
				command = System.getProperty("java.home") + "/bin/java -jar " + jarPath + " -a rv64 -l a.lst -o a.bin ../Assembler/a.asm -x";
			}
			String XmlResult = CommonLib.runCommand(command);
			System.out.println(XmlResult);
			
			XStream xstream = new XStream();
			xstream.processAnnotations(MessageError.class);
			MessageError Error = (MessageError) xstream.fromXML(XmlResult);
	

//			for (hk.quantr.assembler.print.Message e : Error.Messages) {
//					System.out.println(e.type+" "+ e.line+" "+e.message);
//					
//											}
//			System.out.println("/n" + Error.messages.type);
//			System.out.println("/n" + Error.message);
//			
//			System.out.println("/n" + Error.lineNumber);
//			System.out.println("/n" + Error.line);
			
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}
}
